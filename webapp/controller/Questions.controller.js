sap.ui.define([
	"Benchmark/Benchmark/controller/BaseController",
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/m/Dialog',
	'sap/m/Button',
	'sap/m/Text',
	"sap/ui/model/json/JSONModel"
], function(BaseController, Controller, History, Dialog, Button, Text, JSONModel) {
	"use strict";

	return BaseController.extend("Benchmark.Benchmark.controller.Questions", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf Benchmark.Benchmark.view.Questions
		 */

		allQuestions: [], // contains all questions based on filter category an stream
		arrayOfIndexesToExclude: [], // question allreadt randomized
		randomQuestions: [], // selected question;
		index: 0, //number of questions
		i: 0,

		onInit: function() {
			this.getRouter().getRoute("questions").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function() {
			if (!this.oUser.UserID) {
				this.getRouter().navTo("master", {}, true);
				return;

			}
			/*	if (!this.oCategory.CategoryID) {
					this.getRouter().navTo("master", {}, true);
					return;
				}*/
if (this.randomQuestions.length > 0){
	this.index = 0;
	this._HandleQuestion();
} else {
			this._GetQuestion();
}
		},

		_GetQuestion: function() {

			// get the questions;
			this.getModel().read("/Questions", {
				success: this._onSucc.bind(this)
			});
		},

		_onSucc: function(oData) {
			// fill the question array
			var that = this;

			jQuery.each(oData.results, function(i, question) {
				i++;
				that.randomQuestions.push({
					"UserID": that.oUser.UserID,
					"QuestionID": question.QuestionID,
					"ScoreType": question.ScoreType,
					"SchaalID": question.SchaalID,
					"Answer": 0,
					"AnswerID": i
				});

			});

			this._CreateEverything();

		},
		_CreateEverything: function() {
			// now create a model from the selected question
			this._CreateModel();
			// now add the fields to ui
			this._Display();

		},

		_CreateModel: function() {
			var oODataJSONModel = new JSONModel();
			oODataJSONModel.setData({
				Questions: this.randomQuestions
			});
			this.getView().setModel(oODataJSONModel, "RandomQuestions");
			sap.ui.getCore().setModel(oODataJSONModel, "RandomQuestions");

		},
		_Display: function() {
			var oModel = this.getModel("RandomQuestions");
			// get the table.
			var oTable = this.getId("Questions");
			// set the model to table
			oTable.setModel(oModel);

			// put question to screen
			this._HandleQuestion();

		},
		onContinue: function() {
			this._HandleQuestion();

		},
		
		_openRetakeDialog: function(){
			var that = this;
			var dialog = new Dialog({
					title: "Retake test?",
					type: 'Message',
					content: new Text({
						text: "If you press OK, you will lose your current progress"
					}),
					beginButton: new Button({
						text: 'OK',
						press: function() {
							that.getRouter().navTo("RouteView1", {}, true);
						}
					}),
					endButton: new Button({
					text: "Cancel",
					press: function(){
						dialog.close();
						
					}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});	
				dialog.open();
		},
		
		onPrevious: function() {
			if (this.index === 5){
				//this._openRetakeDialog();
				this.getRouter().navTo("profile", {}, true);
				
			} else {
			// set the previous 5
			this.index = this.index - 10;
			this._HandleQuestion();
}
		},
		_HandleButtons: function() {
			//check if had page
			if (this.index > 0) {
				this.getId("BackQuestion").setVisible(true);
			}
		},

		_HandleRandy: function() {
				if (this.index === 20){
					this.getId("randyImg").removeStyleClass("onboardingIMG");
					this.getId("randyImg").addStyleClass("engineerIMG");
				} else {
					this.getId("randyImg").addStyleClass("onboardingIMG");
					this.getId("randyImg").removeStyleClass("engineerIMG");
				}

			switch (this.index) {
				case 5:
					// this.getId("BackQuestion").setText("Retake");
					// this.getId("BackQuestion").setIcon("sap-icon://restart");
					
					this.getId("progressIndicator").setPercentValue(20);
					this.getId("randyText").setText(this.getResourceBundle().getText("questions.randy.page1"));
					this.getId("randyImg").setSrc("img/Randy_notes.png");
					break;
				case 10:
					this.getId("progressIndicator").setPercentValue(40);
					this.getId("randyText").setText(this.getResourceBundle().getText("questions.randy.page2"));
					this.getId("randyImg").setSrc("img/Randy_armscrossed.png");
					break;
				case 15:
					this.getId("progressIndicator").setPercentValue(60);
					this.getId("randyText").setText(this.getResourceBundle().getText("questions.randy.page3"));
					this.getId("randyImg").setSrc("img/Randy_point.png");
					break;
				case 20:
					this.getId("progressIndicator").setPercentValue(80);
					this.getId("randyText").setText(this.getResourceBundle().getText("questions.randy.page4"));
					this.getId("randyImg").setSrc("img/Randy_engineer.png");
					
					break;
				case 25:
					this.getId("progressIndicator").setPercentValue(100);
					this.getId("randyText").setText(this.getResourceBundle().getText("questions.randy.page5"));
					this.getId("ContQuestion").setText("submit");
					this.getId("randyImg").setSrc("img/Randy_Thumbsup.png");
					break;
				default:
					break;

			}
			if (this.index !== 25) {
				this.getId("ContQuestion").setText("continue");
			}

		},

		_HandleIndex: function() {
			var nextindex = this.index + 5;

			// protect max questions
			if (nextindex > this.randomQuestions.length) {
				nextindex = this.randomQuestions.length;
			} else if (this.index < 0) {
				this.index = 0;
				nextindex = this.randomQuestions.length;
			}

			return nextindex;

		},
		_HandleQuestion: function() {
			var oTable = this.getId("Questions");
			// remove all the items 
			oTable.removeAllItems();

			// handle the nav buttons
			this._HandleButtons();

			// where do we start counting
			var oNextindex = this._HandleIndex();

			// oke do we have max question
			if (this.index >= 40) { //parseInt(this.oCategory.MaxQuestion)) {
				this._DefineScore();
				return;
			}

			while (this.index < oNextindex) {
				var sPath = "/Questions(" + this.randomQuestions[this.index].QuestionID + ")";
				var oModel = this.getModel().getProperty(sPath);
				var sValue = "RandomQuestions>/Questions/" + this.index + "/Answer";
				var oAnswer;
				//
				if (oModel.Type === "option") {
					oAnswer = this._CreateSegmentButtons(sValue);
				} else {
					oAnswer = new sap.m.RatingIndicator({
						iconSize: "3rem"
					}).bindProperty("value", sValue);

				}
				var oLabel = new sap.m.Label({
					text: oModel.Question,
					wrapping: true,
					width: "100%",
					editable: false
				});

				var oColumnListItem = new sap.m.ColumnListItem({
					vAlign: "Middle",
					cells: [oLabel, oAnswer]
				});

				// add the table
				oTable.addItem(oColumnListItem);
				this.index++;
			}
			this._HandleRandy();
			// check if thatble has rows.
			if (!oColumnListItem) {
				this._DefineScore();
			}

		},

		_CreateSegmentButtons: function(sPath) {

			var buttonTrue = new sap.m.SegmentedButtonItem({
				text: "{i18n>questions.semanticButton.true}",
				key: "true"
			});

			var buttonFalse = new sap.m.SegmentedButtonItem({
				text: "{i18n>questions.semanticButton.false}",
				key: "false"
			});

			var buttonNoOpinion = new sap.m.SegmentedButtonItem({
				text: "{i18n>questions.semanticButton.noOpinion}",
				key: "Skip",
				visible: false
			});

			if (this.getModel("RandomQuestions").getProperty(sPath.split(">")[1]) === 0) {
				this.getModel("RandomQuestions").setProperty(sPath.split(">")[1], "Skip");
			}

			var oSegmentedButton = new sap.m.SegmentedButton({
				items: [buttonTrue, buttonNoOpinion, buttonFalse],
				// items: [buttonTrue, buttonFalse],
				selectedKey: {
					path: sPath
				},
				selectionChange: [this._Option, this]
			});

			oSegmentedButton.bindSelectedKey(sPath);
			return oSegmentedButton;

		},

		_DefineScore: function() {
			var oEntry = this.getModel("RandomQuestions").oData.Questions;
			var that = this;

			oEntry.forEach(function(Answer) {
				if (Answer.ScoreType === "1") {
					switch (Answer.Answer) {
						case 1:
							Answer.Answer = 1;
							break;
						case 2:
							Answer.Answer = 2;
							break;
						case 3:
							Answer.Answer = 5;
							break;
						case 4:
							Answer.Answer = 8;
							break;
						case 5:
							Answer.Answer = 10;
							break;
						default:
							Answer.Answer = 1;
					}
				} else if (Answer.ScoreType === "2") {
					switch (Answer.Answer) {
						case 1:
							Answer.Answer = 10;
							break;
						case 2:
							Answer.Answer = 8;
							break;
						case 3:
							Answer.Answer = 5;
							break;
						case 4:
							Answer.Answer = 2;
							break;
						case 5:
							Answer.Answer = 1;
							break;
						default:
							Answer.Answer = 1;
					}
					
					//================================
					//Yes or No Questions
					//================================
				} else if (Answer.ScoreType === "3") {
					switch (Answer.Answer.toString()) {
						case "true":
							Answer.Answer = 8;
							break;
						case "false":
							Answer.Answer = 1;
							break;
						case "Skip":
							Answer.Answer = 0;

					}
				} else if (Answer.ScoreType === "4") {
					switch (Answer.Answer.toString()) {
						case "true":
							Answer.Answer = 1;
							break;
						case "false":
							Answer.Answer = 8;
							break;
						case "Skip":
							Answer.Answer = 0;

					}
				}
					//================================
					// End of Yes or No Questions
					//================================
				Answer.Answer = Answer.Answer.toString();

				var defAnswer = {
					"UserID": Answer.UserID,
					"QuestionID": Answer.QuestionID,
					"Answer": Answer.Answer,
					"AnswerID": Answer.AnswerID,
					"SchaalID": Answer.SchaalID
				};
			
				that.getModel().create("/Answers", defAnswer, null, function() {});
			});

			this._HandleDone();
		},

		_HandleDone: function() {
			// first hide the Continue button and table
			this.getId("ContQuestion").setVisible(false);
			this.getId("BackQuestion").setVisible(false);
			this.getId("Questions").setVisible(false);

			// submit the changes
			//	var oEntry = this.getModel("RandomQuestions").oData.Questions;
			/*	var that = this;

			oEntry.forEach(function(Answer) {
				Answer.Answer = Answer.Answer.toString();
				that.getModel().create("/Answers", Answer, null, function() {});
			});
*/

			// Now go to the last page
			this.getRouter().navTo("SendMark", {}, true);

		}

	});

});