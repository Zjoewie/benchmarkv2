sap.ui.define([
	"Benchmark/Benchmark/controller/BaseController",
	'sap/m/Dialog',
	'sap/m/Button',
	'sap/m/Text'
], function(BaseController, Dialog, Button, Text) {
	"use strict";

	return BaseController.extend("Benchmark.Benchmark.controller.OnBoarding", {

		onInit: function() {
			var oModel = this.getModel();
			this.getView().setModel(oModel);

		},

		onPressOnboard: function() {

			if (this._LastCheck()) {
				return;
			}

			if (!this.getId("legalCheck").getSelected()) {
				return;
			}

			// lets create the User.
			this._GetUserInfo();
			this.getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},
		_onMetadataLoaded: function() {
			this._getLatest("User");
		},

		_LastCheck: function() {
			var oForm = this.getId("naw").getContent();

			var sError = false;

			oForm.forEach(function(Field) {
				if (typeof Field.getValue === "function") {

					if (!Field.getValue() || Field.getValue().length < 1) {
						Field.setValueState("Error");

						sError = true;

					}
					else {
						Field.setValueState("None");
					}

				}

			});
			return sError;

		},

		CheckRequired: function(oEvent) {
			//==========================
			// Easter Egg
			//==========================
			if (this.getId("FirstName").getValue() === "3a$t3r 3gg") {

				var soalogo = this.getId("soalogo");

				soalogo.$().animate({
					rotation: 360
				}, {
					duration: 10000,
					step: function(now) {
						$(this).css({
							"transform": "rotate(" + now + "deg)"
						});
					}
				});

			}
			if (this.getId("FirstName").getValue() === "Barrel Roll") {

				var soapage = this.getId("soapage");

				soapage.$().animate({
					rotation: 360
				}, {
					duration: 10000,
					step: function(now) {
						$(this).css({
							"transform": "rotate(" + now + "deg)"
						});
					}
				});

			}
			//==========================
			// End Of Easter Egg
			//==========================
			
			var aInputs = [this.getId(oEvent.getSource().getId())];
			var sError = false;

			jQuery.each(aInputs, function(i, input) {
				if (!input.getValue() || input.getValue().length < 1) {
					input.setValueState("Error");
					input.focus();
					sError = true;
				} else {
					input.setValueState("None");
				}
			});
			return sError;

		},

		/*	_CheckComboBoxes: function(){
			this.getId("Title").	
			},*/
		onSelectLegal: function() {
			this.getId("startBenchmark").setEnabled(this.getId("legalCheck").getSelected());
			this.oUser.OptInTandC = "x";
		},
		_GetUserInfo: function() {
			if (this.oUser.UserID) {
				this.oUser = [];
			}

			var oForm = this.getId("naw").getContent();
			var oViewId = this.getView().getId();
			var that = this;

			oForm.forEach(function(Field) {
				if (typeof Field.getValue === "function") {
					that.oUser[Field.getId().replace(oViewId + "--", "")] = Field.getValue();
				} else {
					try {
						that.oUser[Field.getId().replace(oViewId + "--", "")] = Field.getSelectedItem().getText();
					} catch (err) {
						var afk;
					}
				}

			});

		},
		_getLatest: function(sPath) {
			var that = this;
			var oId = sPath + "ID";
			this.getModel().read("/UserID", {
				sorters: [new sap.ui.model.Sorter(oId, true)],
				urlParameters: ['$top=1'],
				success: function(odata) {
					that._Create(parseInt(odata.results[0][oId], 10) + 1, sPath);
				}
			});

		},
		_Create: function(oNumber, sPath) {
			// prevent creating user on backbuttons
			if (!this.oUser[sPath + "ID"] > 0) {
				this.oUser.CreateDate = new Date();
				this.oUser[sPath + "ID"] = oNumber;
				// create an entry of the user collection with the specified properties and values
				 this.getModel().create("/User", this.oUser, null, function() {});
				// submit the changes (creates entity at the backend)    
				 this.getModel().submitChanges();
			}
			//
			
			this.getModel().attachEventOnce("batchRequestCompleted", function() {
				this.getRouter().navTo("profile");
			}, this);

		},

		onMessageDialogPress: function() {
			var dialog = new Dialog({
				title: this.getView().getModel("i18n").getResourceBundle().getText('login.dialog.legalheader'),
				type: 'Message',
				content: new Text({
					text: this.getView().getModel("i18n").getResourceBundle().getText('login.dialog.legaltext')
				}),
				beginButton: new Button({
					text: 'OK',
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		}

	});
});