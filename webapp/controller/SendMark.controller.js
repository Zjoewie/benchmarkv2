sap.ui.define([
	"Benchmark/Benchmark/controller/BaseController",
	"Benchmark/Benchmark/res/js/intlTelInput",
	"Benchmark/Benchmark/res/js/utils",
	"sap/ui/model/json/JSONModel",
	'sap/m/Dialog',
	'sap/m/Button',
	'sap/m/Text',
	'sap/m/Link',
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter", "sap/ui/model/FilterOperator"
], function(BaseController, intlTelInputjs, utilsjs, JSONModel, Dialog, Button, Text, Link, Controller, History, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("Benchmark.Benchmark.controller.SendMark", {

		totalScore: 0,
		averageSchaal: [],
		scoredText: "",
		rankText: "",

		onInit: function() {
			var phoneinputter = this.getId('phoneInputNew');
			this.getRouter().getRoute("SendMark").attachPatternMatched(this._onRouteMatched, this);
			//this.getId("phoneInput").setMask("+" + this.getView().byId("comboPhoneCodes").getSelectedKey() + "-(0)CCCCCCCCC");

			jQuery.sap.includeStyleSheet(jQuery.sap.getResourcePath("Benchmark/Benchmark/res/css/demo.css"), "demo");
			jQuery.sap.includeStyleSheet(jQuery.sap.getResourcePath("Benchmark/Benchmark/res/css/intlTelInput.css"), "demo");

			phoneinputter.addEventDelegate({
				onAfterRendering: function() {
					phoneinputter.$().find('input').intlTelInput({
						dropdownContainer: 'body',
						initialCountry: "nl",
						onlyCountries: this.countryList,
						placeholderNumberType: "MOBILE",
						preferredCountries: ["nl", "be", "lu", "fr", "de"],
						seperateDialCode: false,
						utilsScript: "res/js/utils.js"

					});
				}
			});
			this.addLinks();
		},

		onLink: function() {
			window.location.replace("https://www.soapeople.com/our-solutions/industry/wholesale-distribution");
		},
		
		onCheck: function(){
		var submitButton = this.getId("startProfile");
		
		if (submitButton.getEnabled() === false){
			submitButton.setEnabled(true);
		} else {
			submitButton.setEnabled(false);
		}
		},
		_onRouteMatched: function() {
			if (!this.oUser.UserID) {
				this.getRouter().navTo("master", {}, true);
				return;

			}

			this._GetScore();

		},
		validate: function()

		{
			var valid = false;
			var email = this.getView().byId("emailInput").getValue();

			var mailregex = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;

			if (!mailregex.test(email)) {

				var dialog = new Dialog({
					title: "Email is not valid",
					type: 'Message',
					content: new Text({
						text: email + " is not a valid email address"
					}),
					beginButton: new Button({
						text: 'OK',
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();
				this.getView().byId("emailInput").setValueState(sap.ui.core.ValueState.Error);
				valid = true;
			} else {
				this.getView().byId("emailInput").setValueState("None");
			}

			return valid;
		},
		_Filter: function() {
			var aFilters = [];

			aFilters.push(new Filter("UserID", FilterOperator.EQ, this.oUser.UserID));

			return aFilters;
		},

		_GetScore: function() {

			var oModel = this.getView().getModel();

			oModel.read("/Answers", {
				filters: this._Filter(),
				success: this._CheckFilled.bind(this)
			});
		},

		_CheckFilled: function(oData) {
			if (oData.results.length === 0) {
				this._GetScore();
			} else {
				this._CalculateScore(oData);
			}
		},
		_CalculateScore: function(oData) {

			var that = this;

			jQuery.each(oData.results, function(i, answer) {
				that.totalScore = that.totalScore + parseInt(answer.Answer);

			});
			that.totalScore = that.totalScore / oData.results.length;
			var answersArray = oData.results;
			this._showText(that.totalScore);
			this.mathTrick(answersArray);

		},
		_setTexts: function() {
			//Labels
			this.getId("scoreLabelSchaal1").setText(this.getResourceBundle().getText("advice.label.1"));
			this.getId("scoreLabelSchaal2").setText(this.getResourceBundle().getText("advice.label.2"));
			this.getId("scoreLabelSchaal3").setText(this.getResourceBundle().getText("advice.label.3"));
			this.getId("scoreLabelSchaal4").setText(this.getResourceBundle().getText("advice.label.4"));
			this.getId("scoreLabelSchaal5").setText(this.getResourceBundle().getText("advice.label.5"));
			//Texts
			this.getId("scoreTextSchaal1").setText(this.getResourceBundle().getText("advice.text.1"));
			this.getId("scoreTextSchaal2").setText(this.getResourceBundle().getText("advice.text.2"));
			this.getId("scoreTextSchaal3").setText(this.getResourceBundle().getText("advice.text.3"));
			this.getId("scoreTextSchaal4").setText(this.getResourceBundle().getText("advice.text.4"));
			this.getId("scoreTextSchaal5").setText(this.getResourceBundle().getText("advice.text.5"));
		},
		
		_showText: function(totalScore) {
			this.getId("score").setText("Your score = " + totalScore.toString() + "%");
			this._setTexts();
			var i = 0;
			if (totalScore >= 0 && totalScore <= 1.99) {

				i = 1;

			} else if (totalScore >= 2 && totalScore <= 3.99) {
				i = 2;

			} else if (totalScore >= 4 && totalScore <= 6.99) {
				i = 3;

			} else if (totalScore >= 7 && totalScore <= 8.49) {
				i = 4;

			} else if (totalScore >= 8.5 && totalScore <= 10) {
				i = 5;

			}
			this._openScoreText(i);
		},

		_openScoreText: function(i) {

			this.getId("scoreTextSchaal" + i.toString()).setVisible(true);
			var textje = this.getId("scoreTextSchaal" + i.toString()).getText();
			this.scoredText = textje;
			this.getId("score").setText(textje);
			var cScoreLabel = this.getId("scoreLabelSchaal" + i.toString());
			this.rankText = this.getResourceBundle().getText("advice.rank." + i.toString());
			this.getId("rankLabel").setText("Digitalisation level: " + this.rankText);
			this.rankText = this.getResourceBundle().getText("advice.label." + i.toString());
			cScoreLabel.addStyleClass("benchmarkScoreName scoreSelected");
		},

		onpress: function() {

			if (this.validate()) {
				return;
			}
var sSucces = 0;
			var scoretest = this.totalScore.toString();
			var sEmailAddress = this.getId("emailInput").getValue();
			var that = this;
			var promise = new Promise(function() {
				$.ajax({
					type: "GET",
					headers: {
						"ToMailAddress": sEmailAddress,
						"USERID": that.oUser.UserID,
						"MailBodyScore": scoretest,
						"ScoreLevel": that.rankText,
						"ScoreText": that.scoredText.replace(/\r\n|\n|\r/gm, '<br />'),
						"Scale1": that.averageSchaal.Schaal1,
						"Scale2": that.averageSchaal.Schaal2,
						"Scale3": that.averageSchaal.Schaal3,
						"Scale4": that.averageSchaal.Schaal4,
						"Scale5": that.averageSchaal.Schaal5

					},

					url: "/SAPCloudIntegration/http/BM001/SendMail",
					dataType: "text",
					success: function() {

					},
					error: function() {


					}
				});
			});
			promise.then(function() {}, function() {});
			
			
			var phoneNumber = this.getId("phoneInputNew").$().find('input').intlTelInput("getNumber");
			this.oUser.Email = sEmailAddress;
			this.oUser.Phone = phoneNumber;
			this.oUser.OptInEmail = "x";
			this.getModel().create("/UserLeads", this.oUser, null, function() {});
			
			
			this.getModel().submitChanges();

			this._ThanksMessage();

		},

		_ThanksMessage: function() {
			var that = this;
			var dialog = new Dialog({
				title: "Thank You!",
				type: 'Message',
				content: new Text({
					text: that.getResourceBundle().getText("email.send")  
				}),
				endButton: new Button({
				type: "Accept",
				text: "Proceed",
				press: function(){
					that.onLink();
				}
				}),
			
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		
		_validatePopUp: function() {
			var that = this;
			var dialog = new Dialog({
				title: "Warning",
				type: 'Message',
				content: new Text({
					text: "Are you sure you want to leave this page? \n\nYour current data will be lost"  
				}),
				beginButton: new Button({
				type: "Reject",
				text: "Go back",
				press: function(){
					dialog.destroy();
				}
				}),
				endButton: new Button({
				type: "Accept",
				text: "Proceed",
				press: function(){
					that.onLink();
				}
				}),
			
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		mathTrick: function(totalScore) {

			this.averageSchaal = {
				"Schaal1": 0,
				"Schaal1Length": 0,
				"Schaal2": 0,
				"Schaal2Length": 0,
				"Schaal3": 0,
				"Schaal3Length": 0,
				"Schaal4": 0,
				"Schaal4Length": 0,
				"Schaal5": 0,
				"Schaal5Length": 0
			};
			var averages = this.averageSchaal;

			var oScores = totalScore;
			oScores.forEach(function(oScore) {
				switch (oScore.SchaalID) {
					case 1:
						averages.Schaal1 = averages.Schaal1 + parseInt(oScore.Answer);
						averages.Schaal1Length = averages.Schaal1Length + 1;
						break;
					case 2:
						averages.Schaal2 = averages.Schaal2 + parseInt(oScore.Answer);
						averages.Schaal2Length = averages.Schaal2Length + 1;
						break;
					case 3:
						averages.Schaal3 = averages.Schaal3 + parseInt(oScore.Answer);
						averages.Schaal3Length = averages.Schaal3Length + 1;
						break;
					case 4:
						averages.Schaal4 = averages.Schaal4 + parseInt(oScore.Answer);
						averages.Schaal4Length = averages.Schaal4Length + 1;
						break;
					case 5:
						averages.Schaal5 = averages.Schaal5 + parseInt(oScore.Answer);
						averages.Schaal5Length = averages.Schaal5Length + 1;
						break;
				}
			});

			averages.Schaal1 = Math.round((averages.Schaal1 / averages.Schaal1Length) * 2);
			averages.Schaal2 = Math.round((averages.Schaal2 / averages.Schaal2Length) * 2);
			averages.Schaal3 = Math.round((averages.Schaal3 / averages.Schaal3Length) * 2);
			averages.Schaal4 = Math.round((averages.Schaal4 / averages.Schaal4Length) * 2);
			averages.Schaal5 = Math.round((averages.Schaal5 / averages.Schaal5Length) * 2);

			var defScore = averages.Schaal1 + averages.Schaal2 + averages.Schaal3 + averages.Schaal4 + averages.Schaal5;
			defScore = Math.round(defScore);
			this.totalScore = defScore;
			this.getId("score").setText("Your Score is: " + defScore + "/100pt");

		},
		
		onMessageDialogPress: function() {
			var dialog = new Dialog({
				title: this.getView().getModel("i18n").getResourceBundle().getText('report.dialog.optinheader'),
				contentWidth:'450px',
				type: 'Message',
				content: new Text({
					text: this.getView().getModel("i18n").getResourceBundle().getText('report.dialog.reportoptintext')
				}),
				beginButton: new Button({
					text: 'OK',
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		
		onMessageDialogNewsPress: function() {
			var dialog = new Dialog({
				title: this.getView().getModel("i18n").getResourceBundle().getText('report.dialog.optinheader'),
				contentWidth:'450px',
				type: 'Message',
				content: new Text({
					text: this.getView().getModel("i18n").getResourceBundle().getText('report.optin.news')
				}),
				beginButton: new Button({
					text: 'OK',
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		}

	});

});