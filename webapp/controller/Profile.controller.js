sap.ui.define([
	"Benchmark/Benchmark/controller/BaseController",
	'sap/m/Link'

], function(BaseController, Link) {
	"use strict";

	return BaseController.extend("Benchmark.Benchmark.controller.Profile", {

		onInit: function() {

			//this.getRouter().getRoute("profile").attachPatternMatched(this._onRouteMatched, this);
			var oModel = this.getModel();
			this.getView().setModel(oModel);
			this.addLinks();
		},

		onContinue: function() {

			this.getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},

		_onMetadataLoaded: function() {
			this._getLatest("User");
		},

		_getLatest: function(sPath) {
			var that = this;
			var oId = sPath + "ID";
			this.getModel().read("/UserID", {
				sorters: [new sap.ui.model.Sorter(oId, true)],
				urlParameters: ['$top=1'],
				success: function(odata) {
					that._Create(parseInt(odata.results[0][oId], 10) + 1, sPath);
				}
			});

		},
		_Create: function(oNumber, sPath) {
			// prevent creating user on backbuttons
			if (!this.oUser[sPath + "ID"] > 0) {
				this.oUser.CreateDate = new Date();
				this.oUser[sPath + "ID"] = oNumber;
				// create an entry of the user collection with the specified properties and values
				this.getModel().create("/User", this.oUser, null, function() {});
				// submit the changes (creates entity at the backend)    
				this.getModel().submitChanges();
			}
			this.getRouter().navTo("questions", {}, true);
		},

		onPrevious: function() {
			this.getRouter().navTo("RouteView1", {}, true);
		},

		_onRouteMatched: function() {
			if (this.oUser.FirstName === "Gangster") {
				this.getId("randyImg").setSrc("img/Randy_G.png");

			}
			if (!this.oUser.UserID) {
				this.getRouter().navTo("master", {}, true);
			}

		}

	});

});